import Vue from 'vue'
import { ApiSearch, } from "../../config"
import apiCall from '../../helpers/apiCall'

const SearchModule = {
    state: {
      searchData: {}
    },

    getters: {
        searchResult(state){
            return state.searchData;
        }
    },
    
    mutations: {
        SET_SEARCH_DATA(state, data){
            state.searchData = data;
        }
    },

    actions:{
        getSearch({ commit }, querySearch) {
            let url = `${ApiSearch}?query=${querySearch}`
            apiCall("get", url)
            .then(res => {
                if (res.data && res.data.code === 0 && res.data.message && res.data.message === "Success") {
                    commit('SET_SEARCH_DATA', res.data.data)
                    Vue.toasted.show(res.data.message, {className: 'bg-success text-white',duration: '2000'});
                }
                else {
                    Vue.toasted.show(res.data.message,{className: 'bg-danger text-white',duration: '2000'});
                
                }
            })
            .catch(error => {
                console.error(error);
            });
        },
        getSearchCustomer({ commit }, customerSearch) {
            let url = `${ApiSearch}?query=${customerSearch}&type=customer`
            apiCall("get", url)
            .then(res => {
                if (res.data && res.data.code === 0 && res.data.message && res.data.message === "Success") {
                    commit('SET_SEARCH_DATA', res.data.data)
                    Vue.toasted.show(res.data.message, {className: 'bg-success text-white',duration: '2000'});
                }
                else {
                    Vue.toasted.show(res.data.message,{className: 'bg-danger text-white',duration: '2000'});
                }
            })
            .catch(error => {
                console.error(error);
            });
        }
    }
};

export default SearchModule;