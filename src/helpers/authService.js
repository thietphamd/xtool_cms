import session from "./sessionHelper";
import { ssn } from "../config";
const authStorageId = ssn.auth;
const roleStorageId = ssn.roles;
const profileStorageId = ssn.profile;

const isAuthenticated = () => !!session.getItem(authStorageId);
const setAuth = (auth) => {
  if (!auth) {
    session.removeItem(authStorageId);
  } else {
    session.setItem(authStorageId, auth);
  }
};

const getAuth = () => {
  const auth = session.getItem(authStorageId);
  return auth != null ? auth : null;
};

const setRoles = (roles) => {
  if (!roles) {
    session.removeItem(roleStorageId);
  } else {
    session.setItem(roleStorageId, roles);
  }
};

const getRoles = () => {
  const roles = session.getItem(roleStorageId);
  return (roles != null && roles.length) ? roles : [];
};

const setProfile = (profile) => {
  if (!profile) {
    session.removeItem(profileStorageId);
  } else {
    session.setItem(profileStorageId, profile);
  }
};

const getProfile = () => {
  const profile = session.getItem(profileStorageId);
  return (profile != null) ? profile : null;
};

const authService = {
  isAuthenticated,
  setAuth,
  getAuth,
  setRoles,
  getRoles,
  setProfile,
  getProfile
};

export default authService;
