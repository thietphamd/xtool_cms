
import {
    URLHome, 
    
    URLUsers, 
    URLCMSUsers,
    URLRoles,
    URLPermissions,
   
    URLBrands,
 
    URLCategories,
    
    URLPages,
    URLVersionBuild,
    URLFeedbacks,


    URLStyles,

    URLProfile,
    URLBuilders,
    URLBuilderCollections,
    URLPlans,
    URLProjects,
    //URLTextures,
    URLMaterials,
    URLTenantUsers,
    URLAssets,
    URLAssetsStyleFit

} from "./config";

//ROle Group from config.ROLES
const Menu = [
    {
        name: 'Home',
        icon: 'fa fa-th-large',
       // translate: 'sidebar.nav.DASHBOARD',
        path: URLHome
    },
    {
        name: 'Users',
        icon: 'fas fa-user-tie',
        //path: URLUsers,
        //roleGroup: 'User',
        submenu: [
            {
                name: 'Account',
                path: URLTenantUsers,
                roleGroup: 'TenantUser'
            },
            {
                name: 'My Profile',
                path: URLProfile
            },
            // {
            //     name: 'CMS Role',
            //     path: URLRoles,
            //     roleGroup: 'Role'
            // },
            // {
            //     name: 'CMS Permission',
            //     path: URLPermissions,
            //     roleGroup: 'Permission'
            // }
        ]
       // translate: 'sidebar.nav.PROFILE'
    },

    {
        name: 'Resources',
        icon: 'fa fa-list',
        outsite: false,
        submenu: [
            {
                name: 'Categories',
                path: URLCategories,
                roleGroup: 'Category'
            },
            {
                name: 'Brands',
                path: URLBrands,
                roleGroup: 'Brand'
            },
            {
                name: 'Builders',
                path: URLBuilders,
                roleGroup: 'Builder'
            },
            {
                name: 'Builder Collections',
                path: URLBuilderCollections,
                roleGroup: 'BuilderCollection'
            },
            {
                name: 'Plans',
                path: URLPlans,
                roleGroup: 'Plan'
            },
            {
                name: 'Projects',
                icon: 'fa fa-folder-open',
                path: URLProjects,
                roleGroup: 'Project'     
            },
        ]
    },

    {
        name: 'Style Fit',
        icon: 'fa fa-edit',
        outsite: false,
        submenu: [
            // {
            //     name: 'Textures',
            //     path: URLTextures,
            //     roleGroup: 'Texture'
            // },
            {
                name: 'Materials',
                path: URLMaterials,
                roleGroup: 'Material'    
            },
            {
                name: 'Assets',
                path: URLAssets,
                roleGroup: 'Asset'    
            },
            {
                name: 'Assets Style Fit',
                path: URLAssetsStyleFit,
                roleGroup: 'AssetStyleFit'    
            }
        ]
    },

   

    {
        name: 'Styles',
        icon: 'fa fa-snowflake',
        path: URLStyles,
        roleGroup: 'Style'    
    },
    
    {
        name: 'Settings',
        icon: 'fa fa-cog',
        outsite: false,
        submenu: [
            {
                name: 'Page',
                //icon: 'fa fa-folder-open',
                path: URLPages,
                roleGroup: 'Page'
            },
            {
                name: 'Build Version Panel',
                path: URLVersionBuild,
                roleGroup: 'Version'
            },

            {
                name: 'User Feed Back',
                path: URLFeedbacks,
                roleGroup: 'Feedback'
            }
        ]
    },
   
];

export default Menu;