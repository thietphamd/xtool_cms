import Vue from 'vue'
import Router from 'vue-router'

import authService from "./helpers/authService";

const Login = () => import("@/views/Auth/Login.vue");
const Profile = () => import("@/views/Auth/Profile.vue");
// Search MAIN
const SearchPage = () => import('@/views/Search/SearchPage.vue')


// Roles
const Roles = () => import('@/views/Roles/ListView.vue')
const Permissions = () => import('@/views/Permissions/ListView.vue')

// Dashboard MAIN
const Home = () => import('@/views/Home/Home.vue')
const Objects = () => import('@/views/Objects/ListView.vue')

// Bundles
const Bundles = () => import('@/views/Bundles/ListView.vue')

// Rooms
const Rooms = () => import('@/views/Rooms/ListView.vue')

// Layouts
const Layouts = () => import('@/views/Layouts/ListView.vue')





//Styles
const Styles = () => import('@/views/Styles/ListView.vue')

//Pages
const Pages = () => import('@/views/Pages/ListView.vue')

//Feedbacks
const Feedbacks = () => import('@/views/Feedbacks/ListView.vue')

//Build Version
const BuildVersion = () => import('@/views/Version/ListView.vue')


/**
 * TENANT CMS
 */
const TenantUsers = () => import('@/views/TenantUsers/ListView.vue')
const TenantUserDetail = () => import('@/views/TenantUsers/DetailView.vue')
//Brands
const Brands = () => import('@/views/Brands/ListView.vue')
const BrandDetail = () => import('@/views/Brands/DetailView.vue')

//Categories
const Categories = () => import('@/views/Categories/ListView.vue')
const CategoryDetail = () => import('@/views/Categories/DetailView.vue')

//Builders
const Builders = () => import('@/views/Builders/ListView.vue')
const BuilderDetail = () => import('@/views/Builders/DetailView.vue')

//Builder Collections
const BuilderCollections = () => import('@/views/BuilderCollections/ListView.vue')
const BuilderCollectionDetail = () => import('@/views/BuilderCollections/DetailView.vue')

//Plans
const Plans = () => import('@/views/Plans/ListView.vue')
const PlanDetail = () => import('@/views/Plans/DetailView.vue')

//Textures
const Textures = () => import('@/views/Textures/ListView.vue')
const TextureDetail = () => import('@/views/Textures/DetailView.vue')

//Materials
const Materials = () => import('@/views/Materials/ListView.vue')
const MaterialDetail = () => import('@/views/Materials/DetailView.vue')

//Assets
const Assets = () => import('@/views/Assets/ListView.vue')
const AssetDetail = () => import('@/views/Assets/DetailView.vue')

//Assets Style Fit
const AssetsStyleFit = () => import('@/views/AssetsStyleFit/ListView.vue')
const AssetStyleFitDetail = () => import('@/views/AssetsStyleFit/DetailView.vue')

//Projects
const Projects = () => import('@/views/Projects/ListView.vue')
const ProjectDetail = () => import('@/views/Projects/DetailView.vue')
/**
 * END 
 */

// Layouts
import Layout from '@/components/Layout/Layout'
// import LayoutHorizontal from '@/components/Layout/LayoutHorizontal'
// import LayoutPage from '@/components/Layout/LayoutPage'
import Empty from "@/components/Layout/Empty";

Vue.use(Router)
import {
  URLPublic, URLLogin, URLProfile,
  URLHome,  
  
  
  URLRoles, 
  URLPermissions,
  URLSearch, 
  URLObjects, URLObjectTTS, URLObjectItems, 
  URLBundles,
  URLRooms,
  URLLayouts,
  URLBrands,
  
 
  URLPages,
  URLVersionBuild,
  URLFeedbacks,
  URLMaterials,

 

  URLStyles,

  URLTenantUsers, 
  URLCategories,
  URLProjects,
  URLBuilders,
  URLBuilderCollections,
  URLPlans,
  URLTextures,
  URLAssets,
  URLAssetsStyleFit,

  ROLES
} from "./config";


const router = new Router({
    mode: "history",
    base: URLPublic,
    routes: [
      {
        path: "/",
        redirect: URLHome
      },
      {
        path: "/",
        component: Layout,
        beforeEnter: (to, from, next) => {
          // ...
          if (authService.isAuthenticated()) {
            next();
          } else {
            next({ path: URLLogin });
          }
        },
        children: [
            //  Home
            {
                path: URLHome,
                component: Home
                
            },
            {
              path: URLProfile,
              component: Profile
              
            },
            {
              component: SearchPage,
              path: URLSearch,
              meta : {
                roles : ROLES.Page
              }
            },
            // Users
            // {
            //     component: Users,
            //     path: URLUsers,
            //     meta : {
            //       roles : ROLES.User
            //     }
            // },

            {
              component: TenantUsers,
              name: "TenantUser",
              path: URLTenantUsers,
              meta : {
                roles : ROLES.TenantUser
              }
            },

            {
              component: TenantUserDetail,
              name: "TenantUserDetail",
              path:  `${URLTenantUsers}/:id`,
              meta : {
                roles : ROLES.TenantUser
              },
            },

            {
              component: Roles,
              path: URLRoles,
              meta : {
                roles : ROLES.Role
              }
            },
            {
              component: Permissions,
              path: URLPermissions,
              meta : {
                roles : ROLES.Permission
              }
            },
            
            //  Object
            {
              path: URLObjectTTS,
              component: Objects,
              props: (route) => ({ type: 'tts' }) ,
              meta : {
                roles : ROLES.Object
              }
            },
            {
              path: URLObjectItems,
              component: Objects,
              props: (route) => ({ type: 'item' }),
              meta : {
                roles : ROLES.Object
              }
              
            },

            // Bundles
            {
              component: Bundles,
              path: URLBundles,
              meta : {
                roles : ROLES.Bundle
              }
            },

            // Rooms
            {
              component: Rooms,
              path: URLRooms,
              meta : {
                roles : ROLES.Room
              }
            },

            // Layouts
            {
              component: Layouts,
              path: URLLayouts,
              meta : {
                roles : ROLES.Layout
              }
            },

            // Brand
            {
              component: Brands,
              name: "Brand",
              path: URLBrands,
              meta : {
                roles : ROLES.Brand
              },
              children: [
                
              ]
            },
            {
              component: BrandDetail,
              name: "BrandDetail",
              path:  `${URLBrands}/:id`,
              meta : {
                roles : ROLES.Brand
              },
            },

            // Category
            {
              component: Categories,
              name: "Category",
              path: URLCategories,
              meta : {
                roles : ROLES.Category
              }
            },
            {
              component: CategoryDetail,
              name: "CategoryDetail",
              path:  `${URLCategories}/:id`,
              meta : {
                roles : ROLES.Category
              },
            },

            // Builder
            {
              component: Builders,
              name: "Builder",
              path: URLBuilders,
              meta : {
                roles : ROLES.Builder
              }
            },
            {
              component: BuilderDetail,
              name: "BuilderDetail",
              path:  `${URLBuilders}/:id`,
              meta : {
                roles : ROLES.Builder
              },
            },

            // Builder Collection
            {
              component: BuilderCollections,
              name: "BuilderCollection",
              path: URLBuilderCollections,
              meta : {
                roles : ROLES.BuilderCollection
              }
            },
            {
              component: BuilderCollectionDetail,
              name: "BuilderCollectionDetail",
              path:  `${URLBuilderCollections}/:id`,
              meta : {
                roles : ROLES.BuilderCollection
              },
            },

            // Plan
            {
              component: Plans,
              name: "Plan",
              path: URLPlans,
              meta : {
                roles : ROLES.Plan
              }
            },
            {
              component: PlanDetail,
              name: "PlanDetail",
              path:  `${URLPlans}/:id`,
              meta : {
                roles : ROLES.Plan
              },
            },
            
            //Texture
            {
              component: Textures,
              name: "Texture",
              path: URLTextures,
              meta : {
                roles : ROLES.Texture
              }
            },
            {
              component: TextureDetail,
              name: "TextureDetail",
              path:  `${URLTextures}/:id`,
              meta : {
                roles : ROLES.Texture
              },
            },

            //Material
            {
              component: Materials,
              name: "Material",
              path: URLMaterials,
              meta : {
                roles : ROLES.Material
              }
            },
            {
              component: MaterialDetail,
              name: "MaterialDetail",
              path:  `${URLMaterials}/:id`,
              meta : {
                roles : ROLES.Material
              },
            },

            //Asset
            {
              component: Assets,
              name: "Asset",
              path: URLAssets,
              meta : {
                roles : ROLES.Asset
              }
            },
            {
              component: AssetDetail,
              name: "AssetDetail",
              path:  `${URLAssets}/:id`,
              meta : {
                roles : ROLES.Asset
              },
            },

            //Asset Style Fit
            {
              component: AssetsStyleFit,
              name: "AssetStyleFit",
              path: URLAssetsStyleFit,
              meta : {
                roles : ROLES.AssetStyleFit
              }
            },
            {
              component: AssetStyleFitDetail,
              name: "AssetStyleFitDetail",
              path:  `${URLAssetsStyleFit}/:id`,
              meta : {
                roles : ROLES.AssetStyleFit
              },
            },


            //Project
            {
              component: Projects,
              name: "Project",
              path: URLProjects,
              meta : {
                roles : ROLES.Project
              }
            },
            {
              component: ProjectDetail,
              name: "ProjectDetail",
              path:  `${URLProjects}/:id`,
              meta : {
                roles : ROLES.Project
              },
            },

            
            //Style
            {
              component: Styles,
              path: URLStyles,
              meta : {
                roles : ROLES.Style
              }
            },

            // Page
            {
              component: Pages,
              path: URLPages,
              meta : {
                roles : ROLES.Page
              }
            },

            // Material
            {
              component: Materials,
              path: URLMaterials,
              meta : {
                roles : ROLES.Material
              }
            },

            // Feedback
            {
              component: Feedbacks,
              path: URLFeedbacks,
              meta : {
                roles : ROLES.Feedback
              }
            },

            // Build Version
            {
              component: BuildVersion,
              path: URLVersionBuild,
              meta : {
                roles : ROLES.Version
              }
            }
        ]
      },
      {
        path: URLLogin,
        component: Empty,
        children: [
          {
            path: "",
            component: Login
          }
        ]
      },
      // Not found route
      {
        path: "*",
        redirect: "/"
      }
    ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const meta = to.meta;
  
  let roles = authService.getRoles();
  if (meta.length && meta.roles.length) { 
    
    if(meta.roles.some(item => roles.includes(item)) ){
      return next();
    }
    else {
      next('/');
    }
  }else{
  
    next();
  } 
  
});

export default router;