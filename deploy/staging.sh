#!/bin/bash

echo 'cd to WorkPlace'
cd /Users/macintoshhd/WorkPlace/FitIn/fitin-crm-frontend
echo 'Done cd to WorkPlace'

echo 'Git pull'
git pull
echo 'Done Git pull'

echo 'Build FE'
yarn build:dev
echo 'Done Build FE'

echo 'cd to Build'
cd build-dev
echo 'Done cd to Unity'

echo 'zip file'
# now=$(date +"%S-%M-%H-%m-%d-%y")
tar cvf build.tar ./
echo 'zip file'

echo 'cp to Server'
scp -i ~/.ssh/fitin_dev build.tar root@103.56.156.49:/var/www/cs.fitin.vn
echo 'cp to Server'

echo 'unzip on Server'
ssh -i ~/.ssh/fitin_dev -t 'root@103.56.156.49' 'cd "/var/www/cs.fitin.vn"; tar xf build.tar ./; chown -R apache:root ./; chmod -R 755 ./;'


# content of your script